//! Module used to resolve file paths and process them

use config::ServerConfig;
use hyper::{Body, Request, Response};
use std::path::Path;

use document::Document;
use http_error::HttpError;

/// Take a filepath and return a Response with the correct mime type
fn serve_file(path: &Path) -> Result<Response<Body>, HttpError> {
	Ok(
		Document::from_file(path)
			.map_err(|_| HttpError::InternalServerError)?
			.into(),
	)
}

/// Take a url path and search for the appropriate file path.
///
/// This uses quite a bit of complex logic. If the path is a directory try each
/// Index file until a suitable one is found. If the file path is a file then
/// send it. But if the file is not found, try adding some file extensions to
/// try and locate the file. If all else fails, return a 404.
pub fn resolve_request(
	config: &ServerConfig,
	request: &Request<Body>,
) -> Result<Response<Body>, HttpError> {
	let base_path = Path::new(&config.document_root);

	let uri_path = Path::new(request.uri().path())
		.strip_prefix("/")
		.map_err(|_| HttpError::BadRequest)?;

	let file_path = base_path.join(uri_path);

	if file_path.exists() {
		if file_path.is_dir() {
			let index_file = config
				.index_files
				.iter()
				.map(|s| file_path.join(Path::new(s)))
				.find(|path| path.is_file());

			if let Some(path) = index_file {
				serve_file(&path)
			} else {
				Err(HttpError::NotFound)
			}
		} else if file_path.is_file() {
			serve_file(&file_path)
		} else {
			Err(HttpError::InternalServerError)
		}
	} else {
		let alternate_extension = config
			.extensions
			.iter()
			.map(|ext| file_path.with_extension(ext))
			.find(|path| path.is_file());

		if let Some(path) = alternate_extension {
			serve_file(&path)
		} else {
			Err(HttpError::NotFound)
		}
	}
}
