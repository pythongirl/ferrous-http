use mime::Mime;
use document::Document;

pub mod external_transformer;
use self::external_transformer::{ExternalTransformer, ExternalTransformerConfig};

type TransformerError = Box<dyn std::error::Error>;

trait FileTransformer {
  fn transform(&self, input: &Document) -> Result<Document, TransformerError>;
  fn will_accept(&self, mime_type: &Mime) -> bool;
}

struct FileTransformerRegistry {
  transformers: Vec<Box<dyn FileTransformer>>
}

single_error!(pub NoTransformersError, "no suitable transformers found");

impl FileTransformerRegistry {
  fn new() -> FileTransformerRegistry {
    FileTransformerRegistry {
      transformers: Vec::new()
    }
  }

  pub fn register(&mut self, transformer: Box<dyn FileTransformer>) {
    self.transformers.push(transformer);
  }

  fn transform(&self, document: &Document) -> Result<Document, TransformerError> {
    for transformer in self.transformers.iter() {
      if transformer.will_accept(document.content_type()) {
        return Ok(transformer.transform(document)?)
      }
    }
        
    Err(NoTransformersError)?
  }
}


pub fn main() {
  let mut registry = FileTransformerRegistry::new();
  
  let config: ExternalTransformerConfig = serde_yaml::from_str(r#"
    path: /usr/bin/rev
    output_type: text/plain
    input_types: 
    - text/plain
  "#).unwrap();

  let transformer = Box::new(ExternalTransformer::from_config(config).unwrap());
  registry.register(transformer);

  let doc = Document::new(mime::TEXT_PLAIN, b"thing1, thing2, thing3".to_vec());
  
  let output_doc = registry.transform(&doc).unwrap();
  println!("{:?}", String::from_utf8_lossy(output_doc.content()));
}

